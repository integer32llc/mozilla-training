class: center, middle

# Error Handling

---

# Option

```
enum Option<T> {
    Some(T),
    None,
}
```

???

- Imported via the prelude

---

# Result

```
enum Result<T, E> {
    Ok(T),
    Err(E),
}
```

???

- Imported via the prelude

---

# Result vs Option

- `Option` denotes something that is present or not
- `Result` denotes something that could succeed or fail
- `Result<T, ()>` compiles to the same as `Option<T>`
  - Different programmer semantics
  - Easy ways to convert between the two

---

# A potential failure

```
fn fetch_age_from_network() -> Result<u8, NetworkError> {
    if is_monday() {
        Err(NetworkError::OnFire)
    } else {
        Ok(42)
    }
}
```

???

- Error types are commonly enumerations of the failure kinds

---

# Dealing with failure

- Very difficult to ignore errors

```
fn next_birthday() -> u8 {
    fetch_age_from_network() + 1 // Won't compile, yay!
}

fn fetch_age_from_network() -> Result<u8, NetworkError> { ... }
```

---

# Dealing with failure

- You can assert that a failure should never happen

```
fn next_birthday() -> u8 {
    fetch_age_from_network().expect("Can't happen") + 1
}

fn fetch_age_from_network() -> Result<u8, NetworkError> { ... }
```

???

- This panics; the thread and maybe whole program are killed

---

# Dealing with failure

- You can do something different on failure

```
fn next_birthday() -> u8 {
    match fetch_age_from_network() {
        Ok(age) => age + 1,
        Err(why) => {
            println!("It must be the day you were born!");
            0
        }
    }
}

fn fetch_age_from_network() -> Result<u8, NetworkError> { ... }
```

---

# Dealing with failure

- You can get a default value

```
fn next_birthday() -> u8 {
    let age = match fetch_age_from_network() {
        Ok(age) => age,
        Err(_) => 0,
    };

    let age = fetch_age_from_network().unwrap_or(0);

    // If the default value is expensive to create
    let age = fetch_age_from_network().unwrap_or_else(|_| 0);

    age + 1
}

fn fetch_age_from_network() -> Result<u8, NetworkError> { ... }
```

???

- Also works with `Option`

---

# Dealing with failure

- You can propagate the error to the caller

```
fn next_birthday() -> Result<u8, NetworkError> {
    let age = match fetch_age_from_network() {
        Ok(age) => age,
        Err(e) => return Err(e),
    };

    let age = try!(fetch_age_from_network());

    let age = fetch_age_from_network()?;

    Ok(age + 1)
}

fn fetch_age_from_network() -> Result<u8, NetworkError> { ... }
```

???

- Probably most common
- Function signature has to change
- Can only use `try!` or `?` in functions that return `Result` because of the early return

---

# Dealing with failure

- You can propagate the error to the caller

```
fn next_birthday() -> Result<u8, NetworkError> {
    fetch_age_from_network()
        .map(|age| age + 1)
}

fn fetch_age_from_network() -> Result<u8, NetworkError> { ... }
```

---

# Dealing with multiple failures

- Often you have multiple error sources

```
fn next_birthday() -> Result<u8, ???> {
    let age = fetch_age_from_network()?;
    let birthday = fetch_birthday_from_disk()?;

    Ok(age + birthday)
}

fn fetch_age_from_network() -> Result<u8, NetworkError> { ... }
fn fetch_birthday_from_disk() -> Result<u8, DiskError> { ... }
```

---

# Creating your own error

```
enum Error {
    Network(NetworkError),
    Disk(DiskError),
}
```

- There are common traits for an error:
  - [`std::error::Error`](https://doc.rust-lang.org/std/error/trait.Error.html)
  - [`std::fmt::Debug`](https://doc.rust-lang.org/std/fmt/trait.Debug.html)
  - [`std::fmt::Display`](https://doc.rust-lang.org/std/fmt/trait.Display.html)
  - [`std::convert::From`](https://doc.rust-lang.org/std/convert/trait.From.html)

---

# Dealing with multiple failures

- Convert different types to your own

```
fn next_birthday() -> Result<i32, Error> {
    let age = match fetch_age_from_network() {
        Ok(age) => age,
        Err(e) => return Error::Network(e),
    };
    let birthday = match fetch_birthday_from_disk() {
        Ok(birthday) => birthday,
        Err(e) => return Error::Disk(e),
    };

    Ok(age + birthday)
}

fn fetch_age_from_network() -> Result<u8, NetworkError> { ... }
fn fetch_birthday_from_disk() -> Result<u8, DiskError> { ... }
```

---

# Creating your own error

- `From` makes this nicer

```
enum Error {
    Network(NetworkError),
    Disk(DiskError),
}

impl From<NetworkError> for Error {
    fn from(e: NetworkError) -> Error {
        Error::Network(e)
    }
}
```

```
let birthday = match fetch_birthday_from_disk() {
    Ok(birthday) => birthday,
    Err(e) => return From::from(e),
};
```

---

# Dealing with multiple failures

- `try!` / `?` will convert error types for you

```
fn next_birthday() -> Result<i32, Error> {
    let age = fetch_age_from_network()?;
    let birthday = fetch_birthday_from_disk()?;

    Ok(age + birthday)
}

fn fetch_age_from_network() -> Result<u8, NetworkError> { ... }
fn fetch_birthday_from_disk() -> Result<u8, DiskError> { ... }
```

---

# Result type alias

For convenience within your crate:

```
type Result<T> = std::result::Result<T, Error>;

fn next_birthday() -> Result<u8> { ... }
```

???

- You can use the standard one via `std::result::Result`

---

# Macros are for boilerplate

```
#[macro_use] extern crate quick_error;

quick_error! {
    #[derive(Debug)]
    enum Error {
        Network(e: NetworkError) {
            description("A network error occurred")
            display("A network error occurred: {}", e)
            from()
            cause(e)
        }
        Disk(e: DiskError) {
            description("A disk error occurred")
            display("A disk error occurred: {}", e)
            from()
            cause(e)
        }
    }
}
```

---

# Lazy programmer protip

- Use `Box<Error>` instead of creating your own type
- Strings can be used as placeholders
- Highly recommended to use aliases so it's less annoying to change

```
type Error = Box<std::error::Error>;
type Result<T> = std::result::Result<T, Error>;

fn fetch_age_from_network() -> Result<u8> {
    if is_monday() {
        Err("Something's burning".into())
    } else {
        Ok(42)
    }
}
```

---

# What *is* a panic?

- By default `panic!` is implemented like an exception using unwinding
- You can use [`catch_unwind`](https://doc.rust-lang.org/std/panic/fn.catch_unwind.html)
  - This is useful to prevent panics from crossing *into* the FFI boundary
- There's a compile-time option that converts panics into program aborts
  - This is useful in low-level contexts, don't care about recovering

---
class: center, middle

[end of section](index.html)
