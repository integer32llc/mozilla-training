class: center, middle

# Generics, Traits, and Trait Objects

---

# Generics

* `Option<T>`: `Some(T)` or `None`
* `Result<T, E>`: `Ok(T)` or `Err(E)`

## Struct definitions

```
struct SpreadsheetCell<T> {
    contents: T
}

let c1 = SpreadsheetCell { contents: String::from("Red") };
let c2 = SpreadsheetCell { contents: 5291 };
```

???

* Point out declaration before use
* Point out convention of short names, PascalCase
* Not as powerful as templates

---

# Generics can be in:

* Structs
* Enums
* Functions
* Methods
* `impl` block
* Traits
* Type aliases

???

* In functions, basically can't do anything interesting with generic types without trait bounds

---

# Traits

* Like abstract classes, interfaces, protocols
* Share code
* Define common behavior

## Basic trait definition

```
trait Person {
    fn name(&self) -> String;
}
```

---

## Basic trait implementation

```
struct Employee {
    first_name: String,
    last_name: String,
    title: String
}

impl Person for Employee {
    fn name(&self) -> String {
        format!("{} {}, {}",
            self.first_name,
            self.last_name,
            self.title
        )
    }
}
```

---

# Trait bounds

```
fn greeting<T: Person>(entity: &T) -> String {
    format!("Hello, {}!", entity.name())
}
```

OR, Jake prefers:

```
fn greeting<T>(entity: &T) -> String
    where T: Person
{
    format!("Hello, {}!", entity.name())
}
```


???

* Monomorphization
* Explain why `where` syntax is more powerful

---

# Derivable traits

```
#[derive(Debug)]
struct Foo {}
```

* Debug
* Copy/Clone
* PartialEq/PartialOrd/Eq/Ord
* Default
* Hash
* Send/Sync
* Any trait can be implemented to be derivable as of Rust 1.15!

---

# `Iterator` is a trait!

```
trait Iterator {
    type Item;
    fn next(&mut self) -> Option<Self::Item>;

    // NOT ACTUAL IMPLEMENTATION
    fn count(self) -> usize {
        let mut count = 0;
        while let Some(_) = self.next() {
            count += 1;
        }
        count
    }
```

???

* Default implementations that can call the other trait methods
* Defaults can be overridden
* Trait methods without definitions must be implemented
* Associated type

---

# `Fn` is a trait!

```
trait Fn<Args> : FnMut<Args> {
    fn call(&self, args: Args) -> Self::Output;
}

trait FnMut<Args> : FnOnce<Args> {
    fn call_mut(&mut self, args: Args) -> Self::Output;
}

trait FnOnce<Args> {
    type Output;
    fn call_once(self, args: Args) -> Self::Output;
}
```

???

* Supertrait
* Its own set of generics

---

# Orphan rule

* Either the trait or the type you're implementing the trait on must be defined in the crate where you're implementing the trait for the type.
* Ensures forwards compatibility

## Not allowed:

```
impl<T> std::fmt::Display for Vec<T> { ... }
```

???

* If you were allowed, in the future whoever owns the trait or type could provide an implementation for the trait or the type, which would be ambiguous.

---

# Trait objects

* Dynamic dispatch
* Use case: when you don't know all the types that might implement a trait where you want to use the types
* Basically, put a trait behind a pointer
* "Type erasure"
  * Can downcast from trait object to concrete type, but not common

---

## Compared to generics + trait bounds

* Static dispatch (monomorphized)
* Generics can only be filled in with one type at a time
* Closed set of known types that implement a trait - could use an enum for this
  * Imagine `Customer` and `Employee` structs that both implement the `Person` trait
  * How could we have a function that takes a `Vec` of a mix of `Customer` and `Employee`?

---

# Let's try using enums!

```
enum CustomerOrEmployee {
    C(Customer),
    E(Employee)
}

impl Person for CustomerOrEmployee {
    fn name(&self) -> String {
        match *self {
            CustomerOrEmployee::C(ref customer) => {
                customer.name()
            },
            CustomerOrEmployee::E(ref employee) => {
                employee.name()
            },
        }
    }
}
```

---

# Vec&lt;CustomerOrEmployee>

```
fn greet_everyone(everyone: &[CustomerOrEmployee]) -> String {
    everyone.iter()
        .map(|e| format!("Hello, {}!", e.name()))
        .collect::<Vec<_>>()
        .join("\n")
}
```

???

* Constrained to the closed set of the CustomerOrEmployee enum
* Users of your library can't extend
* Have to define an enum for every combination

---

# Vec<&Person>

```
fn greet_everyone(everyone: &[&Person]) -> String {
    everyone.iter()
        .map(|e| format!("Hello, {}!", e.name()))
        .collect::<Vec<_>>()
        .join("\n")
}
```

???

* Really we don't care whether the thing is `Customer` or `Employee`, we only care that it's something that implements `Person`

* Now users of your library can extend and pass in any references to things that implement the trait

---

# Returning trait objects

Example: test fixture generator

```
fn arbitrary_person(random: i32) -> Box<Person> {
    if random % 2 == 0 {
        Box::new(Employee {...})
    } else {
        Box::new(Customer {...})
    }
}
```

???

* for now: `impl trait` is coming 🔜
* Common uses: returning Iterators, Futures

---

## Traits must be "Object Safe" to be Trait Objects

A trait is object safe as long as:

* It does not require `Self` to be `Sized`
* All of its methods are object safe
* No associated functions

Methods are object safe if they don't use the `Self` type (except for the first `self` parameter of methods). If the type is erased, then `Self` doesn't know what it is!

???

* Check out the book for a more formal definition
* `Clone` is not object safe

---

# Trait exercises

- Traits and Generics
  - Implement the `Iterator` trait on a type
  - Write a function that takes the created iterator and any other iterator with a compatible `Item` type

- Trait objects
  - Write a function that takes a list of closure trait objects
  - Return a closure trait object that applies them in order
  - Each closure takes an `i32` and returns an `i32`.
  - Protip: Use `Vec`, `Box`, `Fn`

---
class: center, middle

[end of section](index.html), take a break?
