#include <boost/range/adaptor/filtered.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/algorithm/copy.hpp>

#include <string>
#include <vector>
#include <cctype>
#include <algorithm>
#include <iostream>

int main() {
  std::vector<std::string> names = { "Anna", "Bob", "Clarice" };

  auto long_enough = [](auto &name) { return name.length() > 3; };
  auto downcase = [](auto name) {
    for (auto &c : name) c = std::tolower(c);
    return name;
  };

  // Straight-forward
  //
  std::vector<std::string> changed;
  for (auto name : names) {
    if (long_enough(name)) {
      changed.push_back(downcase(name));
    }
  }

  // Pseudo-iterator
  //
  std::vector<std::string> shorter;
  std::copy_if(names.begin(), names.end(), std::back_inserter(shorter), long_enough);
  std::vector<std::string> changed;
  std::transform(shorter.begin(), shorter.end(), std::back_inserter(changed), downcase);

  // Boost
  std::vector<std::string> changed;
  boost::copy(names |
              boost::adaptors::filtered(long_enough) |
              boost::adaptors::transformed(downcase),
              std::back_inserter(changed));

  for (auto name : names) {
    std::cout << name << std::endl;
  }
  for (auto name : changed) {
    std::cout << name << std::endl;
  }
}
