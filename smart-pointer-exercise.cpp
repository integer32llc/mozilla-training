#include <iostream>
#include <vector>
#include <stdint.h>

using namespace std;

struct Game;

struct Player {
  Player(Game* game, uint32_t health) : game(game), health(health) {
  }

  uint32_t health;
  Game* game;
};

struct Game {
  Player& new_player() {
    players.emplace_back(this, 100);
    return players.back();
  }

  vector<Player> players;
};


int main() {
  Game g;
  g.new_player().health -= 10;
  g.new_player().health -= 20;

  for (auto p : g.players) {
    cout << "player health = " << p.health << endl;
  }

  return 0;
}