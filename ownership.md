class: center, middle

# Ownership

???

What does it mean in C++?
How do you think about it?
What does ownership mean in Rust?

---

# Ownership

* Owner of data is responsible for cleaning up that data
    * Synonyms for "cleaning up": **drop**, deallocate, close, destructor/dtor
* Cleanup happens automatically when the owner goes out of scope
* Resource acquisition is initialization (RAII)

???

* drop is the one you'll hear most in Rust
* deallocate = memory
* close = sockets, files

---

# Ownership Examples

```
fn main() {
    let city = String::from("Toronto");
    println!("We are in {}", city);
}
```

???

* `String::from` allocates some memory on the heap
* That memory is automatically freed when the variable goes out of scope

---

# Move

* Transfer ownership
* Invalidates previous owner
* Semantically, a move copies, but the compiler might optimize that out
* Moving is the default/only behavior on assignment
* Similar to C++ `std::move`

---

# Move Examples

## Rust

```rust
fn main() {
    let x = vec![1, 2, 3];
    let y = x;
    println!("x = {}", x.len());
}
```

???

Can't use x again, invalidated

---

# Move Examples

## C++

```c++
int main() {
  std::vector<int> x = {1, 2, 3};
  auto y = x;
  std::cout << x.size() << std::endl;
}
```

???

* What happens if we push to `x`?
  * `y` and `x` are clones
* What happens if we add `std::move`?
  * `x` is emptied
  * Using `x` is placed in a valid but unspecified state

---

# Move Examples

```rust
fn print_vec(v: Vec<i32>) {
    println!("vector = {:?}", v);
}

fn main() {
    let x = vec![1, 2, 3];
    print_vec(x);
    print_vec(x);
}
```

???

functions can move as well, if they're defined that way

---

# Copy

* Types that are cheap to copy
    * Implicit copy instead of move or explicit clone
* Types that are copyable: primitive types, types built from `Copy`
  types and implementing the `Copy` trait

Example:

```rust
fn main() {
    let x = 1;
    let y = x;
    println!("x = {:?}", x);
}
```

???

* Same as 1st move example except now we're using an i32
* Talk about what cheap means in this context

---

# Clone

To force a deep copy, use `clone`:

```rust
fn main() {
    let x = vec![1, 2, 3];
    let y = x.clone();
    println!("x = {:?}", x);
}
```

???

* Cloning that might copy memory or do other expensive stuff is always explicit
* Copy is always cheap

---

# Borrow

* `&x` creates an immutable borrow/reference
* `&mut x` creates a mutable borrow/reference
* `&x[0..5]` creates a slice: pointer + size
* Rules: many immutable borrows OR one mutable borrow within a scope
* Only valid as long as the owner is valid
* Checked at compile time by The Borrow Checker™

???

* borrowck: your new frenemy
* compile time is a big benefit of Rust! early errors! memory safety!

---

# Borrow examples

```rust
fn print_vec(v: &[i32]) {
    println!("slice = {:?}", v);
}

fn main() {
    let x = vec![1, 2, 3];
    print_vec(&x);
    print_vec(&x);
}
```

???

* Like the ownership example that didn't compile, but now takes a slice instead
* Usually functions/methods borrow
  * exceptions: transforms, for efficiency
* Talk about deref?

---

# Borrow examples

```
struct Person { name: String }

impl Person {
    fn greeting(&self) -> &str {
        let g: String = format!("Hello, {}", self.name);
        &g
    }
}
```

???

Does not compile because data being referenced goes out of scope at the end of the function

---

# Borrow examples

```
let mut list = vec![1, 2, 3];
for item in &list {
    println!("item = {}", item);
    list.push(item + 1);
}
```

???

* no iterator invalidation allowed
* if this was allowed, would be an infinite loop
* if `push` caused a reallocation, would invalidate `item`

---

# Borrow examples

```
let mut list = vec![1, 2, 3];
for item in &mut list {
    println!("item = {}", item);
    list.push(*item + 1);
}
```

???

only one mutable borrow allowed

---

# Borrow exercise

```
use std::collections::HashMap;

fn main() {
    let mut game_score = HashMap::new();
    game_score.insert("Carol", 100);

    // Print out the score for "Jake", if there is one.
    // If there isn't a score for "Jake", insert 0 and
    // print out that.
}
```

???

* This is kind of mean.
* We're setting you up for failure.
* This is a common situation in which people fight with the borrow checker.
* Hint: look in the std lib API docs for `HashMap`
* Don't look at the next slide if you don't want to be spoiled.

---

# Possible solution that won't compile

```
use std::collections::HashMap;

fn main() {
    let mut game_score = HashMap::new();
    game_score.insert("Carol", 100);

    match game_score.get("Jake") {
        Some(score) => println!("Jake's score is {}", score),
        None => {
            game_score.insert("Jake", 0);
            println!("Jake's score is 0");
        }
    }
}
```

---

# Actual solution that works: `entry` API

```
use std::collections::HashMap;

fn main() {
    let mut game_score = HashMap::new();
    game_score.insert("Carol", 100);

    let score = game_score.entry("Jake").or_insert(0);
    println!("Jake's score is {}", score);
}
```

???

* Could be fixed by non-lexical borrowing
* Entry API avoids doing a second lookup

---

# Lifetimes

- The time period in which something is guaranteed to reside at a particular memory address
    - time = lines of code, scope

???

Notably, it isn't how long a resource is kept around

---

# Lifetime parameters

`'a`

- A kind of generic
- Goes with references
- Specifies relationships between input and output lifetimes
- NOT PRESCRIPTIVE: doesn't change how long anything is valid

???

- Pronounced "tick a"
- Convention is to start at `'a`.
- Keep it short (usually one letter)
- If you have multiple, might be able to use some reasonable name

---

# Lifetime examples

Won't work:

```
fn main() {
    let string1 = String::from("abcd");
    let string2 = String::from("xyz");

    let result = longest(&string1, &string2);
    println!("The longest string is {}", result);
}

fn longest(x: &str, y: &str) -> &str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
```

???

- How long does the returned reference live for?
- The compiler doesn't know

---

# Lifetime examples

Will work:

```
fn main() {
    let string1 = String::from("abcd");
    let string2 = String::from("xyz");

    let result = longest(&string1, &string2);
    println!("The longest string is {}", result);
}

fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
```

???

* does not mean x and y have to have the same lifetime
* does mean the returned reference guaranteed to live at least as long as the input references

---

# Lifetime examples

Will this compile?

```
fn main() {
    let string1 = String::from("long string is long");

    {
        let string2 = String::from("xyz");
        let result = longest(&string1, &string2);
        println!("The longest string is {}", result);
    }
}

fn longest<'a>(x: &'a str, y: &'a str) -> &'a str { ... }
```

???

* yes
* point out that the signature is what's important and what the compiler uses for analysis

---

# Lifetime examples

Will this compile?

```
fn main() {
    let string1 = String::from("long string is long");
    let result = {
        let string2 = String::from("xyz");
        longest(&string1, &string2)
    };
    println!("The longest string is {}", result);
}

fn longest<'a>(x: &'a str, y: &'a str) -> &'a str { ... }
```

???

* no
* EVEN THOUGH the actual returned reference is that of `x`, which should be valid

---

# Lifetime elision

- Specifying explicit lifetimes all the time would be annoying.
- A majority of the time, there's a small set of rules that the compiler can apply.
- You have to be explicit:
  - If none of the rules apply.
  - If the implicit rules aren't what you want.

---

# Lifetime elision rules

- Each elided lifetime in a function’s arguments becomes a distinct
  lifetime parameter.

- If there is exactly one input lifetime, that lifetime is assigned to
  all elided lifetimes in the return values of that function.

- If there are multiple input lifetimes, but one of them is `&self` or
  `&mut self`, the lifetime of `self` is assigned to all elided output
  lifetimes.

---

# Lifetime elision rules

> Each elided lifetime in a function’s arguments becomes a distinct
> lifetime parameter.

Programmer types:

```
fn foo(x: &i32, y: &i32)
```

Compiler interprets as:

```
fn foo<'a, 'b>(x: &'a i32, y: &'b i32)
```

---

# Lifetime elision rules

> If there is exactly one input lifetime, that lifetime is assigned to
> all elided lifetimes in the return values of that function.

Programmer types:

```
fn foo(x: &i32) -> (&i32, &i32)
```

Compiler interprets as:

```
fn foo<'a>(x: &'a i32) -> (&'a i32, &'a i32)
```

---

# Lifetime elision rules

> If there are multiple input lifetimes, but one of them is `&self` or
> `&mut self`, the lifetime of `self` is assigned to all elided output
> lifetimes.

Programmer types:

```
impl Bar {
    fn foo(&self, x: &i32) -> (&i32, &i32)
}
```

Compiler interprets as:

```
impl Bar {
    fn foo<'a, 'b>(&'a self, x: &'b i32) -> (&'a i32, &'a i32)
}
```

---

# Lifetime elision applied

```
fn longest(x: &str, y: &str) -> &str
```

- Each elided lifetime in a function’s arguments becomes a distinct
  lifetime parameter.

- If there is exactly one input lifetime, that lifetime is assigned to
  all elided lifetimes in the return values of that function.

- If there are multiple input lifetimes, but one of them is `&self` or
  `&mut self`, the lifetime of `self` is assigned to all elided output
  lifetimes.

???

- Returning to the `longest` function example
- None of these rules apply
- We have to annotate the lifetimes

---

# The `'static` lifetime

- Is valid for the entire program execution
- Values that are part of the compiled binary.
- Notably, string literals (`&'static str`)

???

- Still descriptive, not prescriptive!

---

# Exercise: Lifetimes

- Create a function that takes:
  - a `book` that is a string slice
  - a list of proper nouns that is a slice of string slices
- And returns the first proper noun from the book
- There should be no allocations

---

# Exercise: Lifetimes

Sample `main` illustrating usage:

```
fn main() {
    let book = "\
Call me Ishmael.
Some other text here.
Ishmael does something, white whale blah blah blah.";

    let proper_nouns = ["whale", "Ishmael"];

    let first = first_proper_noun(book, &proper_nouns);

    println!("{:?}", first);
}
```

---

# Exercise: Lifetimes

* [One solution](https://gitlab.com/integer32llc/mozilla-training/raw/master/ownership-exercise.rs)

---
class: center, middle

[end of section](index.html), take a break?
