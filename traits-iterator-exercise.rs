// Note missing `Copy`; deriving `Default`
#[derive(Debug, Clone, Default)]
struct Names {
    index: u8,
}

impl Iterator for Names {
    type Item = &'static str;

    fn next(&mut self) -> Option<Self::Item> {
        let index = self.index;
        self.index += 1;
        match index {
            0 => Some("Anna"),
            1 => Some("Bob"),
            2 => Some("Carol"),
            _ => None,
        }
    }
}

fn consume<I>(iter: I)
    where I: Iterator<Item = &'static str>
{
    for name in iter {
        println!("->{}<-", name);
    }
}

fn main() {
    consume(Names::default())
}
