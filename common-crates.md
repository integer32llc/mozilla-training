class: center, middle

# Commonly used crates

---

# [itertools](https://crates.io/crates/itertools)

Additional iterator adapters.

- `unique`
- `join`
- `minmax`
- *and many more!*

---

# Multiprocessing

- [crossbeam](https://crates.io/crates/crossbeam)
  - Concurrent primitives and data structures
  - Scoped threads

- [scoped_pool](https://crates.io/crates/scoped_pool)
  - Scoped and unscoped threads
  - Thread pool

- [rayon](https://crates.io/crates/rayon)
  - Work-stealing
  - Parallel iterator based

---

# Asynchronous

- [tokio](https://tokio.rs/)
  - Asynchronous networking
  - Very new but very promising

- [futures](https://crates.io/crates/futures)
  - General asynchronous patterns
  - Shares some concepts with promises

---

# HTTP

- [hyper](https://hyper.rs/)
  - HTTP client and server
  - Tokio support in master, not yet released

- [reqwest](https://crates.io/crates/reqwest)
  - Higher-level HTTP client
  - Built on top of hyper

---

# Web frameworks

- [iron](http://ironframework.io/)
  - Reigning champion
  - Middleware-based

- [rocket](https://rocket.rs/)
  - Very slick
  - Heavy use of code generation; requires nightly

- [nickel](http://nickel.rs/)
  - Express inspired
  - not updated recently

???

- All are built on top of hyper

---

# Error handling

* [quick-error](https://crates.io/crates/quick-error)
  - Macro that makes it easy to write errors
  - Errors can have `PartialEq`, good for tests

* [error_chain](https://crates.io/crates/error-chain)
  * Sets up some error-handling boilerplate
  * Captures and reports the chain of errors that led to an error

---

# Dates and times

* [time](https://crates.io/crates/time)
  * More nice methods for creating `Duration`s
  * Broken down time values
  * `precise_time_ns` used by Servo

* [chrono](https://crates.io/crates/chrono)
  * Superset of `time`
  * Timezone-aware by default

---

# [petgraph](https://crates.io/crates/petgraph)

* Graph data structure types and algorithms
* Adjacency lists using indices, stored in a regular Rust collection
* The Rust collection owns everything

---

# [lazy_static](https://crates.io/crates/lazy_static)

* Lazily evaluated statics that can be initialized at runtime, including function calls and heap allocations
* Also supports attributes and doc comments
* Used in Servo

---

# [rand](https://crates.io/crates/rand)

* Seeds from OS-provided source of randomness (ex: `/dev/urandom`)
* `OsRng` for entropy source for cryptographic purposes
* Traits, algorithms
* Servo wraps this to share one file descriptor

---

# [regex](https://crates.io/crates/regex)

* Parses, compiles, and executes regular expressions
* No look around or backreferences
* Linear time in respect to the size of regex and search text
* Powers the fastest grep implementation IN THE WORLD ([ripgrep](http://blog.burntsushi.net/ripgrep/))

---

# [serde](https://serde.rs/)

* Generic framework for **ser**ializing and **de**serializing data structures
* Attributes for customization
* Supports the usual data formats: JSON, XML, YAML, TOML, many more!

---

# [byteorder](https://crates.io/crates/byteorder)

- Encode and decode numbers from bytes
- Big and little endian
- Extends `Read` and `Write` for convenience

---

# Operating system

- [nix](https://crates.io/crates/nix)
  - Idiomatic Rust bindings

- [winapi](https://crates.io/crates/winapi)
  - Raw bindings

- [core-foundation](https://crates.io/crates/core-foundation)
  - Idiomatic Rust bindings

- [libc](https://crates.io/crates/libc)
  - Raw bindings

---

# Everything else

- [docs.rs](https://docs.rs/)
- [crates.io](https://crates.io/)

---
class: center, middle

[end of section](index.html)
