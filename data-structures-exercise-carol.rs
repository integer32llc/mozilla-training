use std::collections::HashMap;

fn main() {
    let mut order = vec![];
    let mut counts: HashMap<String, usize> = HashMap::new();

    let contents = String::from("\
hello
world
hello");

    for line in contents.lines() {
        let e = counts.entry(line.to_string()).or_insert_with(|| {
            order.push(line.to_string());
            0
        });
        *e += 1;
    }

    for line in order {
        println!("{} {}", counts.get(&line).unwrap(), line);
    }
}
