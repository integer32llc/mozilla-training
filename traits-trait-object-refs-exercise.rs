fn apply_all<'a>(fns: &'a [&Fn(i32) -> i32]) -> Box<Fn(i32) -> i32 + 'a> {
    Box::new(move |mut value| {
        for f in fns {
            value = f(value);
        }
        value
    })
}

fn main() {
    let fns: &[&Fn(i32) -> i32] = &[
        &|v| v + 1,
        &|v| v * v,
        &|v| -v
    ];
    let megafn = apply_all(&fns);
    let result = megafn(1);
    println!("{:?}", result);
}
