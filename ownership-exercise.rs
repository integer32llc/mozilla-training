fn main() {
    let book = "\
Call me Ishmael.
Some other text here.
Ishmael does something, white whale blah blah blah.";

    let proper_nouns = ["whale", "Ishmael"];

    let first = first_proper_noun(book, &proper_nouns);

    println!("{:?}", first);
}

fn first_proper_noun<'a>(book: &'a str, proper_nouns: &[&str]) -> Option<&'a str> {
    proper_nouns.iter()
        .flat_map(|noun| book.find(noun).map(|idx| (noun, idx)))
        .min_by_key(|&(_, idx)| idx)
        .map(|(noun, idx)| &book[idx..(idx + noun.len())])
}
