use std::fs::File;
use std::collections::HashMap;
use std::collections::hash_map::Entry;
use std::io::{Write, BufRead, BufReader};

fn main() {
    let mut f = File::create("/tmp/junk").expect("no create");
    writeln!(f, "hello").expect("no write");
    writeln!(f, "world").expect("no write");
    writeln!(f, "hello").expect("no write");

    // ----

    let f = File::open("/tmp/junk").expect("no open");
    let f = BufReader::new(f);

    let mut counts = HashMap::new();
    let mut seen_it = HashMap::new();

    for line in f.lines() {
        let line = line.expect("unable to read line");

        let x = seen_it.len();
        if let Entry::Vacant(entry) = seen_it.entry(line.clone()) {
            entry.insert(x);
        }

        // if !seen_it.contains_key(&line) {
        //    let x = seen_it.len();
        //    seen_it.insert(line.clone(), x);
        //}

        *counts.entry(line).or_insert(0) += 1;
    }

    let mut sorted: Vec<_> = counts.into_iter().collect();
    sorted.sort_by_key(|&(ref line, _)| seen_it[line]);

    for (k, v) in sorted {
        println!("{} {}", k, v);
    }
}
