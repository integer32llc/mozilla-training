class: center, middle

# Rust Training for Gecko Developers

---

# Install Rust

* Rust installed - stable **1.16.0**

* If you don't have Rust at all, go to [rustup.rs](https://rustup.rs/) and follow instructions

* If you have rustup but haven't updated Rust in a while, `rustup update stable`

* Make sure you're using stable with `rustup default stable`

???

- What OS are people using?

---

# Introductions

```rust
fn main() {
    println!("Hello, world!");
}
```

---

# Carol Nichols

* Co-author of the upcoming second edition of The Rust Programming Language book
* Rust Community Team Member, Tools Team Member
* Maintainer of Rustlings
* Rust Belt Rust Conference Organizer

---

# Jake Goulding

* \#1 in the Rust tag on Stack Overflow
* Creator of the Rust FFI Omnibus
* Maintainer of the jetscii, twox-hash, sxd-document, sxd-xpath, cupid, and peresil crates
* Rust Belt Rust Conference Organizer

---

# Alexis Beingessner

* Gankro
* Former Rust team intern
* Author of [The Nomicon](https://doc.rust-lang.org/nomicon/) and [Learning Rust With Entirely Too Many Linked Lists](http://cglab.ca/~abeinges/blah/too-many-lists/book/)
* Possible Mozilla/Apple double agent

---

# You

* Your name
* One or more of:
    * What you've done with Rust
    * What you like most about Rust
    * What's the most confusing part of Rust
    * What you don't like about Rust

???

Go around the room. Do we want to ask about projects at this point?

What C++ experience / C++ version used?

---

# Goals

* Level you up: become a productive Rustacean
* Give you the words you need to perform further research
    * Turn unknown unknowns into known unknowns
* Provide hands-on mentoring

<img src="http://vignette3.wikia.nocookie.net/scratch-cup/images/7/76/Gotta_go_fast.png/revision/latest?cb=20150123021616" />

???

* Rust, like any language, is broad and has some nooks and crannies that
you might need to know that wouldn't traditionally be covered. We'll
circle back on any shared concerns.
* We're gonna go fast today, we're happy to review anything or go into
more depth on anything tues + wed
* Ready? let's go! Start with defining some terms, concepts, syntax

---

# Rust is safe

... for some definition of safe:

* No null references
* No data races
* No use-after-free
* No double free
* Panics on out-of-bound memory access

---

# Totally safe and can still happen in Rust

* Deadlocks/livelocks
* Memory/resource leaks
* Exiting without calling destructors
* Integer overflows
* Logic bugs
* `panic!`

???

* panicking can happen due to failure to allocate

---

# Useful Trivia

* Familiarity with Cargo, commands `new`, `build`, `run`, `test`
    * Make a scratch project: `cargo new --bin`, `cargo run`
* Documentation locations:
    * [online](https://doc.rust-lang.org/stable/)
    * offline:
        * `rustup component list` to see if you have them
        * `rustup component add rust-docs` if you don't
        * `rustup doc`
    * for all the crates within a project: `cargo doc --open`
* `println!("{:?}", thing);`

???

* Period of time that rustup didn't include docs by default -- that era is now over
* Check that they know about binary+src/main.rs vs library+src/lib.rs

---

# Quick Syntax

* Comments: `//` or `/* */`
* Variables: `let x = 3;`, `let mut y = 5;`
* Primitive types: `i32`, `u32`, `f64`, `&str`, `bool`, `usize`, array, tuple, slice
* Complex types: enum, struct
* Stdlib types: `String`, `Vec<T>`, `HashMap<K, V>`
* Strongly typed; types are inferred most of the time
* `for`/`loop`/`while`
* `Option<T>`: `Some(T)` or `None`
* `Result<T, E>`: `Ok(T)` or `Err(E)`

---

# Quick Syntax

## Conditionals

```
if x > 3 {
    // code
} else if y < 4 {
    // code
} else {
    // code
}
```

---

# Quick Syntax

## Match

```rust
match x {
    0 => println!("nothing"),
    1 => println!("one is the lonliest number"),
    2...4 => println!("a few"),
    _ => println!("a lot!"),
}
```

---

# Quick Syntax

## Structs

```
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}
```

---

# Quick Syntax

## Enums

```
enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}
```

---

# Quick Syntax


## Functions

```rust
fn add_one(x: i32) -> i32 {
    // no semicolon: implicit
    // return of last expression value
    x + 1
}

let six = add_one(5);
```

---

# Quick Syntax

## Methods

```
struct Foo {} // or enum
impl Foo {
    fn bar(self) { // or &self or &mut self
    }
}
let f = Foo {};
f.bar();
```

???

Methods are like functions but within an impl on some type and 1st argument is self

---
class: center, middle

[end of section](index.html)
