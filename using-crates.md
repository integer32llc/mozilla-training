class: center, middle

# Using crates

---

# Standard library philosophy

- Rust strives to keep a small standard library
- Cargo and crates have existed since Rust 1.0
- You need to (and should) use crates

???

- Python's 5 "get data from URL" modules, none of which are what people use, have to be supported forever.
- ["standard library is where things go to die"](http://www.leancrew.com/all-this/2012/04/where-modules-go-to-die/)
- We've heard that Gecko has re-implementations of C++ features, many other C++ projects too

---

# Adding a crate

Add it to the `[dependencies]` section of Cargo.toml:

```toml
[dependencies]
rand = "0.3.15"
```

- Semver-compatible version, not strict equality
- `0.3.20` is compatible, `0.4.0` is not compatible
- `cargo edit` is useful to quickly add the most recent version

???

- This is the default behavior, there are sigils to do different things.
- Ways of specifying more complicated requirements.
- Leftmost non-zero. `"1.2.3"` is compatible with `1.2.5` and `1.3.0`

---

# Consistent builds

- On first build, Cargo downloads and compiles the code
- Updates Cargo.lock with the exact version

???

- Commit Cargo.lock for binaries, not libraries
- Yarn, Bundler, etc.
- Usually no need to hand-edit
- Rust version not tracked yet

---

# Updating dependencies

- `cargo update` checks for versions that are compatible with your requirements
- Modifies Cargo.lock with the new versions

???

- Cargo doesn't upgrade versions willy-nilly, you have to be explicit
- `cargo outdated` can show you which dependencies are old

---

# Dependency sources

- A local path

```toml
rand = { path = "../projects/rand" }
```

- From a git repo

```toml
rand = { git = "https://github.com/rust-lang-nursery/rand" }
```

???

- Git allows specifying branch, tag, commit
- Lockfile still in play

---

# Overriding

- Direct dependency
  - Edit `[dependencies]`
- Indirect or direct dependency
  - Add to `[replace]`
  - Add path to `.cargo/config`

???

- Servo prefers updating and releasing new version, otherwise `[replace]`
- docs/HACKING_QUICKSTART.md
- Carol has used `.cargo/config`, Jake just changes `[dependencies]`
- Can always fork

---

# Using `[replace]`

- Replaces a *specific* version with another.
- Version number in the replacement must exactly match

```toml
[dependencies]
rand = "0.3.15"

[replace]
"libc:0.2.21" = { path = "/tmp/libc" }
```

???

- libc is a dependency of rand, "has a bug"

---

# Actually using the crate

Add an `extern crate` statement at the root of your crate:

```
extern crate rand;

fn main() {
    rand::thread_rng();
}

mod foo {
    use rand;

    fn example() {
        rand::thread_rng();
    }
}
```

???

- Attaches the crate to the module tree wherever the `extern crate` statement is
- Then acts like any other module.

---

# Multiple versions of the same crates

```text
crates-io v0.1.0
├── diesel v0.12.0
│   └── byteorder v1.0.0
└── migrate v0.1.0
    └── postgres v0.13.6
        └── postgres-protocol v0.2.0
            └── byteorder v0.5.3
```

This is totally OK! <sup>Most <sup>of <sup>the <sup>time</sup></sup></sup></sup>

???

- You won't always see this, if Cargo is able to satisfy both with one version, it will
- This is an NP-hard problem (see SAT solvers)
- Might see multiple versions more frequently than you expect

---

# When it's a problem

- Native libraries
  - Cargo has the `link` metadata to catch this
- Exposing a dependency in your API
  - e.g. urlpocalypse, libcpocalypse

```text
error[E0308]: mismatched types
 --> src/main.rs:7:13
  |
7 |     awesome(Foo);
  |             ^^^ expected struct `Foo`, found struct `Foo`
  |
  = note: expected type `Foo`
             found type `Foo`
```

???

- There are efforts to make this less painful
- Not allowing different versions causes different pains
- This is a **real** problem in dynamic languages

---

# More details

- [doc.crates.io](http://doc.crates.io/)
- [Servo's HACKING_QUICKSTART](https://github.com/servo/servo/blob/master/docs/HACKING_QUICKSTART.md)

---
class: center, middle

[end of section](index.html)
