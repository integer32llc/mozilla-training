class: center, middle

# Macros

---

# Macros are all around us

- `println!`
- `format!`
- `panic!`
- `unreachable!`
- `unimplemented!`
- `assert!`
- `vec!`

???

- Wheres `map!`? Which map?

---

# Macros can be invoked in many locations

- Inside of a module
- Inside of a struct / enum
- Inside of a function
- Where ever a type is
- Probably more

---

# Macros are not C macros

- Operate on the AST
- Can't generate invalid-to-parse code

???

- C macros are just text manipulation

---

# The try! macro

```
macro_rules! my_try {
    ($e:expr) => {
        match $e {
            Ok(val) => val,
            Err(e) => return Err(From::from(e)),
        }
    }
}
```

???

- `macro_rules!`
- multiple patterns, first matching
- the types correspond to AST pieces
- variables have `$` to separate them
- Can see why they are called "macros by example"

---

# Macros don't give you new abilities

- Everything you can write with one of these macros you can write by hand
- They just reduce boilerplate

---

# Repetition

```
macro_rules! my_vec {
    ( $($e:expr),* ) => ({
        let mut v = Vec::new();
        $( v.push($e); )*
        v
    })
}
```

???

- Introduce repetition with `$()`
- Can have `*` or `+`
- Comma can be inside (required trailing) or before the repetition (required not trailing)
- Variables defined inside repetition can only be accessed inside inside repetition

---

# Multiple patterns

```
macro_rules! thing {
    ($e1:expr, $e2:expr) => (2);
    ($e1:expr) => (1);
}
```

???

- Semicolons separate multiple patterns

---

# The types

- `ident`: an identifier `x`, `foo`
- `path`: a qualified name `T::SpecialA`
- `expr`: an expression `2 + 2`
- `ty`: a type `Vec<(char, String)>`, `&T`
- `pat`: a pattern. `Some(t)`
- `stmt`: a single statement `let x = 3`
- `block`: sequence of statements and expression
- `item`: an item `fn foo() { }`
- `meta`: a "meta item" `cfg(target_os = "windows"`
- `tt`: a single token tree.

---

# Scoping

- Macros have to be defined before they can be called
- Importing
  - `#[macro_use]` before a crate to import macros
  - `#[macro_use(foo)]` to select which macros
- Exporting
  - `#[macro_export]` on a macro

---

# Debugging

- Sometimes you want to check the expanded code
- `rustc -Z unstable-options --pretty expanded`
- `cargo rustc -- -Z unstable-options --pretty expanded`

---

# Procedural macros

- Sometimes called "Macros 1.1"

```
#[derive(MyCoolThing)]
struct TheStruct;
```

- Basically, the compiler has one stable API:

```
extern crate proc_macro;

#[proc_macro_derive(MyCoolThing)]
pub fn my_cool_thing(input: TokenStream) -> TokenStream { ... }
```

???

- `TokenStream` is just a string
- Used by tools like serde to... avoid writing boilerplate

---

# Macros 2.0

- The *future*
- Lots of things up in the air
- Reason the keyword `macro` is reserved

---
class: center, middle

[end of section](index.html)
