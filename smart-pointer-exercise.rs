use std::rc::{Rc, Weak};
use std::cell::RefCell;

struct Game {
    name: String,
    players: Vec<Rc<RefCell<Player>>>,
}

struct Player {
    name: String,
    health: u32,
    games: Vec<Weak<RefCell<Game>>>,
}

fn join_game(game: &Rc<RefCell<Game>>, player: &Rc<RefCell<Player>>) {
    game.borrow_mut().players.push(player.clone());
    player.borrow_mut().games.push(Rc::downgrade(game));
}

fn print_game_roster(game: &Rc<RefCell<Game>>) {
    let game = game.borrow();
    println!("Game: {}", game.name);
    println!("Players:");
    for player in &game.players {
        println!("- {}", player.borrow().name);
    }
    println!();
}

fn print_player(player: &Rc<RefCell<Player>>) {
    let player = player.borrow();
    println!("Player: {}", player.name);
    println!("Games:");
    for game in &player.games {
        if let Some(game) = game.upgrade() {
            println!("- {}", game.borrow().name);
        }
    }
    println!();
}

fn main() {
    let g1 = Rc::new(RefCell::new(Game {
        name: String::from("chess boxing"),
        players: vec![],
    }));
    let g2 = Rc::new(RefCell::new(Game {
        name: String::from("checkers boxing"),
        players: vec![],
    }));

    let p1 = Rc::new(RefCell::new(Player {
        name: String::from("Jake"),
        health: 100,
        games: vec![],
    }));

    let p2 = Rc::new(RefCell::new(Player {
        name: String::from("Carol"),
        health: 100,
        games: vec![],
    }));

    let p3 = Rc::new(RefCell::new(Player {
        name: String::from("Gankro"),
        health: 100,
        games: vec![],
    }));

    join_game(&g1, &p1);
    join_game(&g2, &p1);

    join_game(&g1, &p2);

    join_game(&g2, &p3);

    println!("Current games:");
    print_game_roster(&g1);
    print_game_roster(&g2);

    println!("\nCurrent players:");
    print_player(&p1);
    print_player(&p2);
    print_player(&p3);
}
