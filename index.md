### Rust Training for Gecko Developers

Slides are at http://mozilla-training.integer32.com

Repo for issues, PRs: https://gitlab.com/integer32llc/mozilla-training

Etherpad: https://public.etherpad-mozilla.org/p/mozilla-rust-training

.pull-left[
TODAY:

1. [Introductions/Prerequisites](intro.html)
1. [Ownership, borrowing, references, and lifetimes](ownership.html)
1. [Data structures, iterators, closures](data-structures.html)
1. [Generics, traits, and trait objects](traits.html)
1. [Smart pointers](smart-pointers.html)
1. [Concurrency](concurrency.html)
1. [Idiomatic projects & module structure](project-structure.html)
1. [Using crates](using-crates.html)
1. [Commonly used crates](common-crates.html)
1. [Error handling](error-handling.html)
1. [Macros](macros.html)
]
.pull-right[
TOMORROW & WEDNESDAY:

1. Working on your projects
1. Sharing practical knowledge

IN THE NEXT TWO WEEKS:

We're available to answer your questions via email, chat, or video chat!
]
