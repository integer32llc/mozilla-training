fn apply_all(fns: Vec<Box<Fn(i32) -> i32>>) -> Box<Fn(i32) -> i32> {
    Box::new(move |mut value| {
        for f in &fns {
            value = f(value);
        }
        value
    })
}

fn main() {
    let megafn = apply_all(vec![
        Box::new(|v| v + 1),
        Box::new(|v| v * v),
        Box::new(|v| -v),
    ]);
    let result = megafn(1);
    println!("{:?}", result);
}