class: center, middle

# Idiomatic projects & module structure

---

# Filesystem structure

```text
src/
├── baz.rs
├── foo
│   ├── bar.rs
│   └── mod.rs
└── lib.rs
```

???

- lib.rs is the entrypoint

---

# lib.rs

.pull-left[
```
mod baz;
mod foo;

mod quux {
    fn inside_quux() {}
}
```
]

.pull-right[
```text
src/
├── baz.rs
├── foo
│   ├── bar.rs
│   └── mod.rs
└── lib.rs     <----
```
]

???

- declare baz, foo, quux
- defining module quux - often used for tests

---

# baz.rs

.pull-left[
```
fn inside_baz() {}
```
]

.pull-right[
```text
src/
├── baz.rs     <----
├── foo
│   ├── bar.rs
│   └── mod.rs
└── lib.rs
```
]

???

- module declared in lib.rs
- file has same name as module + '.rs'
- Rust knows where to look

---

# foo/mod.rs

.pull-left[
```
mod bar;

fn inside_foo() {}
```
]

.pull-right[
```text
src/
├── baz.rs
├── foo
│   ├── bar.rs
│   └── mod.rs <----
└── lib.rs
```
]


???

- module name as directory + 'mod.rs' other place
- needed when you have a nested module (here `bar`)
- `mod bar` is another declaration
- bar is the same as `baz` - just function definition

---

## Filesystem

```text
src/
├── baz.rs
├── foo
│   ├── bar.rs
│   └── mod.rs
└── lib.rs
```

## Modules

```text
example : crate
 ├── baz : private
 ├── foo : private
 │   └── bar : private
 └── quux : private
```

???

- cargo plugin `cargo modules`
- modules basically mirror filesystem
- rules map between the two, you don't willy-nilly name files
- note `private`; visibility is coming up

---

# Unit tests

```
#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn fooing_the_bar() {
        assert_eq!(1, 1);
    }
}
```

???

- usually a module at the bottom of the same file as the methods you are testing
- `cfg(test)` don't compile this unless compiling tests
- `use super::*` brings in everything from that module
- `#[test]` denotes a test function
- `cargo test` is the test runner
- child modules have access to private methods from parent modules
- up to you to decide if you want to test private methods #nojudgement

---

# Visibility

- Everything is private by default
- `pub` makes something public
- functions, types, modules, traits, struct fields
- There needs to be a public path to be used outside the crate
- `pub use` lets you export a different API from your layout

---

# Integration tests

- Tests your library as a consumer might use it
- Uses the same test runner as unit tests
- Your crate must be explicitly included
- Each integration test is its own crate.

**tests/test_name.rs**

```
extern crate example;

#[test]
fn can_create_a_new_user() {
    example::baz::inside_baz();
}
```

???

- `cargo test`
- every file in the directory is a test - careful about modules

---

# Examples

* Executable documentation for how to use your library
* Your crate must be explicitly included
* Each example is its own crate

**examples/example_name.rs**

```
extern crate example;

fn main() {
    example::baz::inside_baz();
}
```

???

- `cargo run --example example_name`
- every file in the directory is an example - careful about modules

---

# Module organization

Unlike some languages, Rust does not have a strong "one thing per
file" convention. Instead, semantically related types are grouped into
modules.

If you strongly believe in keeping separate files, you can use `pub
use` to present an idiomatic public API.

---

# Crate organization: Workspaces

* Many crates in one directory/repo
* Top level has a Cargo.toml with `[workspace]` and optionally lists its members
* Crates in subdirectories are inferred to be part of the workspace
* Output of compiling any crate is relative to the root
* `Cargo.lock` for the whole workspace is in the root
* Each crate is tested and published separately using `cargo -p`
* Makes it easier to keep all the crates in sync

---

# Idiomatic

* Document your code
* Format your code
* Lint your code

---

# Documenting

```
/// Short description of function
///
/// Longer paragraphs describing the function
///
/// # Examples -- how to use it; tested!
/// # Panics   -- when will it panic
/// # Safety   -- what invariants if unsafe
/// # Errors   -- when errors occur
fn the_best_library_method() { /* ... */ }
```

- If you put code in code fences in a doc comment, it will be run with `cargo test`!


???

- `cargo doc` and `cargo doc --open`
- Same tooling as standard library API docs
- Can use Markdown
- These are common headers

---

# Formatting

* There's an implicit Rust style that exists
* The exact details of an explicit Rust style are [still being argued about][fmt-rfcs].
* [Rustfmt][rustfmt] can be installed with `cargo install rustfmt`; you can also install `cargo-fmt`.

[fmt-rfcs]: https://github.com/rust-lang-nursery/fmt-rfcs
[rustfmt]: https://github.com/rust-lang-nursery/rustfmt

---

# Linting

* Rust has built-in warnings, aim to not have false positives
* [Clippy][clippy] has *many* more checks, with a slightly higher risk of false positives.
* Can be a great way to be guided to write idiomatic Rust code.

[clippy]: https://github.com/Manishearth/rust-clippy

---

# Exercise

- Make a library crate with some modules and functions
- Make some public, some private
- Write some unit tests and integration tests
- Export a different public API with `pub use`
- Document your API and view the generated HTML
- Try Rustfmt and Clippy

???

- Simple ideas: math/calculator library; greeting library

---
class: center, middle

[end of section](index.html)
