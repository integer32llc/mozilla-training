class: center, middle

# Data structures, iterators, closures

---

# Standard library data structures

- `String`,
- `Vec`
- `HashMap` and `HashSet`
- `BTreeMap` and `BTreeSet`

[Full list][collections]

[collections]: https://doc.rust-lang.org/std/collections/

---

# Creating your own data structure

- Rust is young, standard library is small
- Many [user-contributed types][crates] are in crates
- Doing low-level data structures is **hard**
- Read [*Entirely Too Many Linked Lists*][linkedlists] first
- Then maybe read [*The Rustonomicon*][nomicon] if you need to go lower

[crates]: https://crates.io/categories/data-structures
[linkedlists]: http://cglab.ca/~abeinges/blah/too-many-lists/book/
[nomicon]: https://doc.rust-lang.org/nomicon/

---

# Graph structures

- Graphs (as opposed to trees) are also hard
- Ownership becomes very muddled; who owns who?

## Alternatives
- Shared ownership (`Rc`; be wary of cycles)
- Index-based ([petgraph][])
- Manual memory management

[petgraph]: https://crates.io/crates/petgraph

---

# Iterators

- Pervasive
- High level
- Efficient

---

# Simple interface

```
fn next(&mut self) -> Option<Self::Item>;
```

Iterators are lazy and do nothing until you call `next`.

---

# Integrated with `for` loops

```
for i in 0..10 {
    println!("{}", i);
}
```

```
let names = vec!["Anna", "Bob", "Clarice"];
for name in names {
    println!("{}", name);
}
```

---

# Iterator methods

```
let changed: Vec<_> = names.iter()
    .filter(|name| name.len() > 3)
    .map(|name| name.to_lowercase())
    .collect();
```

There are [a lot more methods][iter], as well as crates
like [itertools][].

[iter]: https://doc.rust-lang.org/std/iter/trait.Iterator.html
[itertools]: https://crates.io/crates/itertools

???

- `iter`
  - returns an iterator over the item, doesn't take ownership
  - also `into_iter` if you need to take ownership

- `filter` / `map`
  - take a closure to allow configuration

- `collect`
  - builds a collection from the iterator
  - needs type specified

---

# Efficiency

Same code as if written by hand:

```
let mut changed = Vec::new();
for i in 0..names.len() {
    let name = &names[i];
    if name.len() > 3 {
        changed.push(name.to_lowercase());
    }
}
```

???

- Unlike some languages, Rust does not create a vector for each step
- What Rust means by "zero-cost abstractions"
- Iterator might actually be more efficient; hand-written version
  might perform bounds checks on array access
- `Iterator::size_hint` would be very useful (if we weren't filtering)

---

# Closures

Little bits of code that have the ability to capture their environment

```
let day = "Monday";
let greeter = |name| println!("Happy {}, {}", day, name);

greeter("Rust");
greeter("C++");
```

---

# Captured environment

Closures *automatically* capture variables from the environment based
on what happens inside the closure.

```
fn takes_ownership(s: String) {}
fn takes_reference(s: &str) {}

let month = String::from("April");
let day = String::from("Monday");

let example = || {
    takes_ownership(month);
    takes_reference(&day);
};
```

???

- C++ explicit capture-list isn't needed
- What happens if we print `month` at the end? What about `day`?
- What about printing `day` after calling `example`?
- What about calling `example` twice?

C++ can capture a local variable in lambda and return the lambda. Then
the lambda uses bad memory.

---

# Forcing capturing by value

Sometimes, the automatic capture rules aren't right, so you can force
all captured variables to be moved into the closure.

```
fn takes_ownership(s: String) {}
fn takes_reference(s: &str) {}

let month = String::from("April");
let day = String::from("Monday");

let example = move || {
    takes_ownership(month);
    takes_reference(&day);
};
```

???

- Highlight the `move` keyword
- What if we print `day` at the end now?

---

There are three *traits* for closures:

- [`Fn`](https://doc.rust-lang.org/std/ops/trait.Fn.html)
- [`FnMut`](https://doc.rust-lang.org/std/ops/trait.FnMut.html)
- [`FnOnce`](https://doc.rust-lang.org/std/ops/trait.FnOnce.html)

These tell you when and what kind of closure can be passed to a function:

```
fn takes_a_closure<F>(f: F) -> usize
    where F: Fn(&str) -> usize
{
    f("hello") + 1
}

takes_a_closure(|name| name.len());
```

???

- A closure is "just" an implementation of one of these traits on an
  auto-generated struct

- Returning a closure is more annoying, covered under trait objects

---

## Exercise: Count the occurrences of unique lines in a file

- Create a function that takes a filename and prints out the result
- Can hard code the filename passed to that function
- Additions:
  - Take command line arguments for which file(s)
  - Order the output by line; count; line then count; order of initial occurrence
  - Return a value that can be formatted with `{}`

---

# Example input and output

.pull-left[
**Input**

```text
hello
world
hello
```
]
.pull-right[
**Output**

```text
hello 2
world 1
```
]

---

# Exercise solutions

* [Jake](https://gitlab.com/integer32llc/mozilla-training/raw/master/data-structures-exercise-jake.rs)
* [Carol](https://gitlab.com/integer32llc/mozilla-training/raw/master/data-structures-exercise-carol.rs)
* [Using C++ iterators](https://gitlab.com/integer32llc/mozilla-training/raw/master/data-structures-iterator-example.cxx)

---
class: center, middle

[end of section](index.html), take a break?
