class: center, middle

# Concurrency

---

# Ownership rules

* Apply in multithreaded situations as well
* Prevent data races
* Safe
* Easier to get it right
* Concurrency isn't a special case

???

- Perhaps not actually easier to do
- Allows you to focus on the next level up
  - how to structure / split up work
  - is concurrency actually beneficial for your case

---

# Functionality

## Rust the language

* Little knowledge of concurrency
  * Ownership
  * `Send`/`Sync`

???

* Language isn't opinionated

---

# Functionality

## Rust's standard library

* Concurrent data structures
  * `Arc`, `Mutex`, `RwLock`
* Atomic primitives
* `std::thread`
* Channels

???

* Std lib provides building blocks

---

# Functionality

## Crates

* rayon
* crossbeam*
* Various scoped thread pool crates

???

* Crates can enable lots of different stuff
  * Crossbeam is in flux right now, might get split up
* Concentrating on stdlib stuff now

---

# Spawn a thread

```
use std::thread;

fn main() {
    thread::spawn(|| {
        println!("Hello, world!");
    });
}
```

???

* doesn't work, why?
* This isn't guaranteed to run the thread because we're not joining!

---

# Join a thread

```
use std::thread;

fn main() {
    let handle = thread::spawn(|| {
        println!("Hello, world!");
    });

    handle.join().expect("Unable to join");
}
```

???

- Join blocks and waits for the thread to finish

---

# Share data with a thread

```
use std::thread;

fn main() {
    let name = String::from("Carol");

    let handle = thread::spawn(|| {
        println!("Hello, {}!", name);
    });

    handle.join().expect("Unable to join");
}
```

???

* doesn't work, why?
* thread might outlive main
* this closure is only trying to take a reference (that is not 'static)

---

# `std::thread::spawn`

```
pub fn spawn<F, T>(f: F) -> JoinHandle<T>
    where F: FnOnce() -> T,
          F: Send + 'static,
          T: Send + 'static
```

???

* Important part: `'static`
* Any references must have the static lifetime

---

# `move` forces a transfer of ownership

```
use std::thread;

fn main() {
    let name = String::from("Carol");

    let handle = thread::spawn(move || {
        println!("Hello, {}!", name);
    });

    // Won't work because `name` was moved into the thread
    // println!("Hello from main, {}!", name);

    handle.join().expect("Unable to join");
}
```

???

* not really "sharing"

---

# Sharing ownership between threads

```
use std::thread;
use std::sync::Arc;

fn main() {
    let name = Arc::new(String::from("Carol"));

    let sent_name = name.clone();

    let handle = thread::spawn(move || {
        println!("Hello, {}!", sent_name);
    });

    println!("Hello from main, {}!", name);

    handle.join().expect("Unable to join");
}
```

???

* Arc is atomic reference counted
* Clone and then send
* Cloning is explicit, there are no copy constructors
* Immutable only

---

### Sharing mutable data between threads

```
use std::thread;
use std::sync::{Arc, Mutex};

fn main() {
    let name = Arc::new(Mutex::new(String::from("Carol")));

    let sent_name = name.clone();

    let handle = thread::spawn(move || {
        let mut value = sent_name.lock()
            .expect("Couldn't get lock");
        println!("thread got the lock neener neener");
        value.push_str("lllll");
        println!("Hello, {}!", *value);
    });

    let value = name.lock().expect("Couldn't get lock");
    println!("main got the lock");

    println!("Hello from main, {}!", *value);

    handle.join().expect("Unable to join");
}
```

???

* Deadlocks are safe!
* Locks release when they go out of scope
* Solutions: move the join, call drop explicitly, make an inner scope
* RwLock is alternative, allows multiple readers, one writer (sound familiar?)

---

## Returning values from threads

```
use std::thread;
use std::time::Duration;

fn main() {
    let handles: Vec<_> = (0..10).map(|i| {
        thread::spawn(move || {
            thread::sleep(Duration::from_millis(1000));
            i + 1
        })
    }).collect();

    let sum: i32 = handles
        .into_iter()
        .map(|handle| handle.join().expect("Unable to join"))
        .sum();

    println!("The sum was {}", sum);
}
```

???

* How long will this take?
* Running in parallel because it takes 1 second
* Could use shared state to

---

### Sending data with channels

```
use std::thread;
use std::sync::mpsc;

fn main() {
    let (tx, rx) = mpsc::channel();

    let _handles: Vec<_> = (0..10).map(|i| {
        let tx = tx.clone();
        thread::spawn(move || {
            tx.send(i + 1).expect("Unable to send");
        })
    }).collect();
    drop(tx);

    let mut sum = 0;
    for value in rx {
        println!("{}", value);
        sum += value;
    }

    println!("The sum was {}", sum);
}
```

???

- channel receiver acts as an iterator
- done iterating when all tx are dropped (thus explicit `drop`
- values not in order
- joining is optional here (why?)

---

# Atomic variables

```
use std::thread;
use std::sync::Arc;
use std::sync::atomic::{AtomicUsize, Ordering};

fn main() {
    let sum = Arc::new(AtomicUsize::new(0));

    let handles: Vec<_> = (0..10).map(|i| {
        let sum = sum.clone();
        thread::spawn(move || {
            sum.fetch_add(i + 1, Ordering::SeqCst);
        })
    }).collect();

    for handle in handles.into_iter() {
        handle.join().expect("Unable to join");
    }

    let sum = sum.load(Ordering::SeqCst);
    println!("The sum was {}", sum);
}
```

???

- Limited set of atomics; based on hardware.
- Need to specify an ordering
 - Use SeqCst unless you know better
- Still need to share ownership of the atomic variable

---

# Configuring your threads

```
use std::thread;

fn main() {
    let child = thread::Builder::new()
        .name("My thread".into())
        .stack_size(1024 * 1024)
        .spawn(|| println!("Hello from the thread"))
        .expect("Unable to spawn thread");

    child.join().expect("Unable to join thread");
}
```

???

- name
- stack size
- handle failure to spawn

---

# Thread-related traits

## `Send`

Safe to transfer this type between threads.

## `Sync`

Safe to share a reference to this type between threads.

???

- Automatically implemented if your type is built from `Send`/`Sync` types.
- Non-`Send`: raw pointers, `Rc`, thread-locals
- Non-`Sync`: raw pointers, `Rc`, `Cell`s

---

# Scoped threads

```
extern crate crossbeam;

fn main() {
    let name = String::from("Carol");

    crossbeam::scope(|scope| {
        scope.spawn(|| {
            println!("Hello, {}!", name);
        })
    });

    println!("Hello from main, {}!", name);
}
```

???

- Removes the `'static` restriction by guaranteeing that the threads will exit.
- Didn't have to move `name` / use `Arc`, can use `name` afterward
- it's from a crate

---

# Parallel iterators

```
extern crate rayon;

use std::thread;
use std::time::Duration;

use rayon::prelude::*;

fn main() {
    let sum = (0..10)
        .into_par_iter()
        .map(|i| {
            thread::sleep(Duration::from_millis(1000));
            i + 1
        })
        .sum();

    println!("The sum was {}", sum);
}
```

---

# Exercise: Count the occurrences of unique lines in multiple files

- Reuse code from Data Structures section that counts lines in one file
- Create a thread for each file to count
- Additions:
  - Previous additions (command line, ordering, formatting with `{}`)
  - Prevent output from different files from interleaving
  - Aggregate results from all files

???

- What might be different if there was one big file instead of many files?
- Any downsides to using 1 thread per?

---

# Example solution

* [Solution](https://gitlab.com/integer32llc/mozilla-training/raw/master/concurrency-exercise.rs)

---
class: center, middle

[end of section](index.html)
