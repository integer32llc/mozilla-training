use std::collections::HashMap;
use std::thread;
use std::path::Path;
use std::fs::File;
use std::io::Read;

fn main() {
    let files = ["/etc/hosts", "/etc/hosts"];

    let handles: Vec<_> = files.iter().map(|&file| {
        thread::spawn(move || {
            one_file(file)
        })
    }).collect();

    for handle in handles {
        let counts = handle.join().expect("Unable to join thread");
        for (line, count) in counts {
            println!("{} {}", line, count);
        }
    }
}

fn one_file<P>(path: P) -> Vec<(String, usize)>
    where P: AsRef<Path>
{
    let mut f = File::open(path).expect("unable to open");
    let mut s = String::new();
    f.read_to_string(&mut s).expect("unable to read");
    count_lines(&s)
}

fn count_lines(contents: &str) -> Vec<(String, usize)> {
    let mut order = vec![];
    let mut counts = HashMap::new();

    for line in contents.lines() {
        let e = counts.entry(line.to_string()).or_insert_with(|| {
            order.push(line.to_string());
            0
        });
        *e += 1;
    }

    order.into_iter()
        .map(|line| {
            let count = counts[&line];
            (line, count)
        })
        .collect()
}
