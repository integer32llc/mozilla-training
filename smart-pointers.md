class: center, middle

# Smart Pointers

---

# Smart Pointers are

* Blatantly stolen from C++
* Pointer with extra info
* Implements the `Deref` trait
* Often implements the `Drop` trait as well
* `Vec<T>` and `String` are smart pointers in a way

???

* Other things implement `Drop` and aren't smart pointers
* Not all smart pointers customize `Drop`

---

# `Box<T>`

* Allocates on the heap
* Roughly equivalent to `std::unique_ptr`
* Useful in trait objects
* Useful in recursive data types

```
let foo = Box:new(3);
```

---

# Deref trait

## `Deref` trait definition

```
trait Deref {
    type Target;
    fn deref(&self) -> &Self::Target;
}
```

## Deref operator

The `*` operator "expands" to:

```
*(something.deref())
```

---

# Deref coercions

* If you have a type `U`, and it implements `Deref<Target=T>`, values of `&U` will automatically coerce to a `&T` at function/method calls.
* Applies as many times as necessary
* Improves ergonomics

```
fn main() {
    let name = Box::new(String::from("Carol"));
    hi(&name);
}

fn hi(name: &str) {
    println!("hi {}", name);
}
```

---

# `Drop` trait

* Destructor

```
impl Drop for CustomSmartPointer {
    fn drop(&mut self) {
        println!("Dropping CustomSmartPointer!");
    }
}
```

* Compiler inserts a call to `drop` where values go out of scope
* Not allowed to call `drop` explicitly, that would be a double drop

???

* Remember it's not guaranteed to be called if the program exits
* Reference cycles cause memory leaks

---

# `drop` vs. `drop`

* `Drop::drop` != `std::mem::drop`
* You *can* call `std::mem::drop`
* Rarely *need* to

```
mod std {
    mod mem {
        fn drop<T>(x: T) { }
    }
}
```

---

# `Rc<T>`/`Arc<T>`

* Reference Counted (`Arc` is Atomic)
* Roughly equivalent to `std::shared_pointer`
* For multiple ownership of immutable data

---

# `Rc<T>` example

```
use std::rc::Rc;

fn main() {
    let moms_turtle = Rc::new("🐢");
    let kids_turtle = turtle.clone();

    print_animal(&moms_turtle);
}

fn print_animal(animal: &str) {
    println!("I own a {}", animal);
}
```

???

* `Weak<T>`

---

# `RefCell<T>`/`Mutex<T>`

* Runtime checked borrow rules instead of compile time
    * `borrow` and `borrow_mut` methods return `Ref` and `RefMut` smart pointers
    * will panic if you violate the borrowing rules
* Interior mutability
* Mutex is threadsafe
    * `lock` method returns `MutexGuard` smart pointer
    * Lock is released when the guard goes out of scope

---

# `RefCell<T>` example

```
use std::rc::Rc;
use std::cell::RefCell;

fn main() {
    let moms_turtle = Rc::new(RefCell::new(String::from("🐢")));
    let kids_turtle = moms_turtle.clone();

    feed_animal(&mut moms_turtle.borrow_mut());
    println!("{}", *kids_turtle.borrow());
}

fn feed_animal(animal: &mut String) {
    animal.push_str("🍀");
}
```

---

# `Cow<T>` 🐮

* Lazily clone on write
* Variants: `Borrowed(&T)` and `Owned(T)`
* `Deref` for immutable references
* `to_mut` to get a mutable reference, which clones if necessary
* `into_owned` extracts owned value, clones if necessary

---

# `Cow<T>` example

```
use std::borrow::Cow;

fn main() {
    let a1 = Cow::from("🐮"); // This does not allocate
    let a2 = Cow::from(String::from("🐄")); // This allocates

    let an1 = Animal::new(a1); // This allocates
    let an2 = Animal::new(a2); // This does not allocate
}

struct Animal {
    name: String,
}

impl Animal {
    fn new(name: Cow<str>) -> Animal {
        Animal { name: name.into_owned() }
    }
}
```

---

# Smart Pointers in Servo

* [Traced reference to a JS-managed DOM object ](https://github.com/servo/servo/blob/b66839367c40bf2557c721cd964feb91d5cf3287/components/script/dom/bindings/js.rs#L96)


---

# Deref Antipatterns

* Don't use `Deref` to implement inheritance!!!

```
pub struct Keyword(pub String);

impl Deref for Keyword {
    type Target = str;
    fn deref(&self) -> &str { &self.0 }
}
```

* `Keyword` now has all the methods of `&str`... is that the semantics you want???
* Would you subclass the string type???

???

* Borderline example
* From crates.io's code

---

# Smart Pointer exercise

- Is [this C++ code](https://gitlab.com/integer32llc/mozilla-training/raw/master/smart-pointer-exercise.cpp) safe?
- Make a Rust program that:
  - Creates Games that can have Players
  - Creates Players that can be in Games
  - Print out all the games and which players are in the games
  - Print out all the players and which games they're in
  - Hint: use `Rc<T>` and `RefCell<T>`

???

* http://stackoverflow.com/questions/25284837/references-and-lifetimes

---

# Solution discussions

[Possible solution](https://gitlab.com/integer32llc/mozilla-training/raw/master/smart-pointer-exercise.rs)

- Was the task difficult/verbose/tricky in Rust?
- Is this task difficult/verbose/tricky in C++?
- Should it be difficult/verbose/tricky?

---
class: center, middle

[end of section](index.html), take a break?
